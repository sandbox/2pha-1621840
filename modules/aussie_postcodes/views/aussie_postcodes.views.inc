<?php 

/**
 * @file
 * Hooks for Views integration.
 */

/**
 * Implements hook_field_views_data().
 *
 *
 */
function aussie_postcodes_views_data(){
  $data = array(
    'aussie_postcodes' => array(
      'table' => array(
        'group' => t('Aussie postcodes'),
        'base' => array(
          'field' => 'pid',
          'title' => t('Aussie postcodes'),
        ),
      ),
      'pid' => array(
        'title' => t('Pid'),
        'help' => t('the unique postcode pid'),
      ),
      'postcode' => array(
        'title' => t('Postcode'),
        'help' => t('the postcode'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
          'click sortable' => TRUE,
        ),
      ),
      'suburb' => array(
        'title' => t('Suburb'),
        'help' => t('the suburb'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'state' => array(
        'title' => t('State'),
        'help' => t('the state'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'dc' => array(
        'title' => t('Delivery center'),
        'help' => t('delivery center'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'type' => array(
        'title' => t('Type'),
        'help' => t('delivery type'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'lat' => array(
        'title' => t('Latitude'),
        'help' => t('Latitude'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
      ),
      'lon' => array(
        'title' => t('Longitude'),
        'help' => t('Longitude'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
      ),
    ),
  );
  return $data;
}