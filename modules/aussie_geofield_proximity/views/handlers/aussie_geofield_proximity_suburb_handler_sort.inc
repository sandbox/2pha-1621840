<?php

class aussie_geofield_proximity_suburb_handler_sort extends geofield_handler_sort {
  
  // this can not be exposed
  function can_expose() {
    return FALSE;
  }
  
  function query() {
    // get the alias of the field we are sorting on
    $alias = $this->view->display_handler->handlers['field'][$this->options['location']]->field_alias;
    
    // add the sort
    $this->query->add_orderby($this->table, NULL, $this->options['order'], $alias);
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['location'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    if ($handlers = $this->view->display_handler->get_field_labels()) {
      $options = array();
      foreach ($handlers as $name => $handler) {
        // only aussie fields are options
        if (strpos($name, 'aussie_field_geofield_proximity') === 0) {
          $options[$name] = $name;
        }
      }
      $form['location'] = array(
        '#title' => t('Distance from'),
        '#type' => 'select',
        '#default_value' => isset($this->options['location']) ? $this->options['location'] : '',
        '#options' => $options,
        '#description' => t('Select location from which to measure distance.'),
      );
    }

    parent::options_form($form, $form_state);

    // Remove lat and lon distance from options form.
    unset($form['dist_lat']);
    unset($form['dist_lon']);

  }
}
