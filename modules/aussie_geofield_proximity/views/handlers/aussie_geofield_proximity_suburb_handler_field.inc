<?php

class aussie_geofield_proximity_suburb_handler_field extends geofield_handler_field {

  function query() {
     // Get the referenced filter.
    //$filter = $this->options['location'];
    $filter = $this->field;
    // Get the referenced filter hanlder.
    $handler = $this->view->display_handler->get_handler('filter', $filter);
    
    // If the filter is exposed, get the real values:
    if (!empty($this->view->exposed_raw_input['aussie_postcodes']) && (strlen($this->view->exposed_raw_input['aussie_postcodes']) > 0)) {
      $location = $this->view->exposed_raw_input['aussie_postcodes'];
      $radius_of_earth = $this->options['radius_of_earth'];
    }else{
      $location = $this->options['location']['aussie_postcodes'];
      $radius_of_earth = $this->options['radius_of_earth'];
    }
    
    // Geolocate based on this value.
      // @TODO don't geolcoate the same string more than once.  Cache?  Relationship?
    $locdata = aussie_postcodes_get_data($location);
    if($locdata){
      //$lat = $geocoded->getY();
      //$long = $geocoded->getX();
      $lat = $locdata['lat'];
      $lon = $locdata['lon'];
      
      // Get the real geofield.
      $geofield = $this->view->field[$this->real_field];
      //print_r($geofield);
      $this->ensure_my_table();
      $haversine = geofield_haversine($lat, $lon, $geofield->definition['field_name'] . '_lat', $geofield->definition['field_name'] . '_lon', $radius_of_earth);
      $this->field_alias = $this->query->add_field(NULL, $haversine, $this->field);
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['location'] = array('default' => '4000');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    if ($handlers = $this->view->display_handler->get_handlers('filter')) {
      $form['location'] = array(
        '#title' => t('Distance from'),
        '#type' => 'aussie_postcodes',
        //'#options' => $options,
        '#default_value' => isset($this->options['location']['aussie_postcodes']) ? $this->options['location']['aussie_postcodes'] : '',
        '#description' => t('Select location from which to measure distance.'),
      );
    }
    
    
    // Remove lat and lon distance from options form.
    unset($form['dist_lat']);
    unset($form['dist_lon']);

  }
  
  function get_value($values, $field = NULL) {
    if (isset($values->{$this->field})) {
      //dpm($values->{$this->field});
      return $values->{$this->field_alias};
    }
  }

}
