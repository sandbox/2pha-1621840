<?php

/**
 * @file
 * Hooks for Views integration.
 */


/**
 * Implmentation of hook_field_views_data_alter();
 */
function aussie_geofield_proximity_field_views_data_alter(&$result, $field, $module) {
  if ($module == 'geofield') {
    foreach ($result as $table => $data) {
      $field_name = $field['field_name'];
      if (isset($data[$field_name])) {
        $result[$table]['aussie_field_geofield_proximity_suburb'] = array(
          'group'       => t('Content'),
          'title'       => $data[$field_name]['title'] . ' - Aussie suburb proximity',
          'help'        => $data[$field_name]['help'],
          'filter' => array(
            'field'      => 'aussie_field_geofield_proximity_suburb',
            'handler'    => 'aussie_geofield_proximity_suburb_handler_filter',
            'table'      => $table,
            'field_name' => $field_name,
            'real_field' => $table,
          ),
          'field' => array(
            'field'      => 'aussie_field_geofield_proximity_suburb',
            'handler'    => 'aussie_geofield_proximity_suburb_handler_field',
            'table'      => $table,
            'field_name' => $field_name,
            'real_field' => $table,
            'float'      => TRUE,
            'click sortable' => TRUE,
          ),
          'sort' => array(
            'field'      => 'aussie_field_geofield_proximity',
            'handler'    => 'aussie_geofield_proximity_suburb_handler_sort',
            'table'      => $table,
            'field_name' => $field_name,
            'real_field' => $table,
          ),
        );
      }
    }
  }
}